[![dynamic-http-client MyGet Build Status](https://www.myget.org/BuildSource/Badge/dynamic-http-client?identifier=db8f4a24-e09c-41b4-97d0-5c60d305b0e9)](https://www.myget.org/)
# Dynamic REST Client

A .NET Dynamic HTTP client that uses reflective dispatch to build strongly-typed RESTful client interfaces.